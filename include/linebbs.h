#ifndef __LINEBBS_H__
#define __LINEBBS_H__

#include <stdint.h>

#define LBBS_VERSION 1.306

#define const_write(sock, msg) write(sock, msg, strlen(msg))

void lbbs_launch(uint16_t port, char* config);

#endif
