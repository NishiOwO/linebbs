CC := cc
CFLAGS := -g -std=c89 -I./include -fPIC

ifeq ($(shell uname -s), SunOS)
	CFLAGS := $(CFLAGS) -lsocket
endif

default: help
all: ./bin/lbbs ./bin/lbbs-static
directory:
	mkdir -p obj
	mkdir -p bin

./bin/lbbs: ./obj/main.o ./obj/lbbs.o
	$(CC) $(CFLAGS) -o $@ $^

./bin/lbbs-static: ./obj/main.o ./obj/lbbs.o
	$(CC) $(CFLAGS) -static -o $@ $^

./obj/main.o: ./src/main.c ./include/linebbs.h
	$(CC) $(CFLAGS) -c -o $@ $<

./obj/lbbs.o: ./src/linebbs.c ./include/linebbs.h
	$(CC) $(CFLAGS) -c -o $@ $<

help:
	@echo "all       | Builds everything."
	@echo "directory | Creates a directories which are needed."
	@echo "help      | Displays this message."
