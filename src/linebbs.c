#define _BSD_SOURCE

#include <linebbs.h>

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <dirent.h>
#include <sys/wait.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

struct {
	uint16_t port;
	char* welcome;
	char* password_database_path;
	char* banned_database_path;
	char* workplace_path;
	char* mail_path;
	char* contact;
	char* le_path;
	char* warn;
	char* top_text;
	char create_account;
} lbbs_info;

bool lbbs_exists(char* path){
	FILE* f = fopen(path, "r");
	if(f == NULL){
		return false;
	}else{
		fclose(f);
		return true;
	}
}

void zombie_killer(int sig){
	int status;
	int pid;
	while((waitpid(-1, &status, WNOHANG)) > 0);
}

mode_t lbbs_mode(char* path){
	struct stat s;
	if(stat(path, &s) == 0){
		return s.st_mode;
	}else{
		return 0;
	}
}

void clear(int client){
	char* chars = malloc(3);
	free(chars);
	const_write(client, "\x1b[2J");
	const_write(client, "\x1b[1;1H");
	const_write(client, "\x1b[0m");
}

char* edit_path;

void editor(int client, char* username){
	int fd[2];
	int fd_out[2];
	pipe(fd);
	pipe(fd_out);
	int pid = fork();
	if(pid == 0){
		close(fd[1]);
		close(fd_out[0]);
		dup2(fd[0], STDIN_FILENO);
		dup2(fd_out[1], STDOUT_FILENO);
		dup2(fd_out[1], STDERR_FILENO);
		chdir(edit_path);
		remove(username);
		printf("sh/shell command is disabled.\r\n");
		execlp(lbbs_info.le_path, lbbs_info.le_path, strdup(username), NULL);
		_exit(0);
	}else{
		close(fd[0]);
		close(fd_out[1]);
		char* buf = malloc(1);
		char* buffer = malloc(0);
		int counter = 0;
		if(fork() == 0){
			char buf2[1];
			while(1){
				if(1 > read(fd_out[0], buf2, 1)) _exit(0);
				if(buf2[0] == '\n'){
					write(client, "\r\n", 2);
				}else{
					write(client, buf2, 1);
				}
			}
			_exit(0);
		}
		while(1){
			if(kill(pid, 0) != 0) break;
			if(1 > read(client, buf, 1)) _exit(0);
			if(kill(pid, 0) != 0) break;
			if(buf[0] == '\r'){
				char* _tmp = malloc(1);
				read(client, _tmp, 1);
				free(_tmp);
				write(client, "\r\n", 2);
				write(fd[1], buffer, counter);
				write(fd[1], "\n", 1);
				counter = 0;
				free(buffer);
				buffer = malloc(0);
			}else if(buf[0] == 8 || buf[0] == 0x7f){
				if(counter > 0){
					counter--;
					buffer[counter] = 0;
					const_write(client, "\x08 \x08");
				}
			}else{
				write(client, buf, 1);
				buffer = realloc(buffer, counter + 2);
				buffer[counter] = buf[0];
				buffer[counter + 1] = 0;
				counter++;
			}
		}
		waitpid(pid, 0, 0);
		/*
		 * read(client, buf, 1);
		 */
		free(buffer);
		free(buf);
	}
}

char* lbbs_date(){
	char* date = malloc(128);
	time_t t = time(NULL);
	struct tm *time_st = localtime(&t);
	strftime(date, 128, "%a %b %e %H:%M:%S %Z %Y", time_st);
	return date;
}

#define WAIT_UNTIL_KEY 	if(1 > read(client, key, 1)) break;\
			if(key[0] == '\r'){\
				read(client, key, 1);\
			}

#define REFRESH	clear(client);\
		goto refresh;

char* sstrcat(const char* str1, const char* str2){
	char* result = malloc(strlen(str1) + strlen(str2) + 1);
	result[strlen(str1) + strlen(str2)] = 0;
	strcpy(result, str1);
	strcpy(result + strlen(str1), str2);
	return result;
}

char* sstrcat3(const char* str1, const char* str2, const char* str3){
	char* _str = sstrcat(str1, str2);
	char* result = sstrcat(_str, str3);
	free(_str);
	return result;
}

void lbbs_more(int client, const char* path){
	FILE* f = fopen(path, "r");
	int x = 0;
	int y = 0;
	char* chr = malloc(1);
	if(f == NULL) return;
	while(!feof(f)){
		if(1 > fread(chr, 1, 1, f)) break;
		char c = chr[0];
		x++;
		if(c == '\n'){
			write(client, "\r\n", 2);
			x = 0;
			y++;
		}else{
			write(client, chr, 1);
		}
		if(x == 79){
			write(client, "\r\n", 2);
			x = 0;
			y++;
		}
		if(y == 23){
			const_write(client, "-- More --");
			char* key = malloc(1);
			WAIT_UNTIL_KEY;
			free(key);
			clear(client);
			x = 0;
			y = 0;
		}
	}
	free(chr);
	fclose(f);
}

#define EDIT_ASK	{\
				char* buf = malloc(1);\
				char* buffer = malloc(0);\
				int counter = 0;\
				while(1){\
					if(1 > read(client, buf, 1)) _exit(0);\
					if(buf[0] == '\r'){\
						char* _tmp = malloc(1);\
						read(client, _tmp, 1);\
						free(_tmp);\
						counter = 0;\
						break;\
					}else if(buf[0] == 0x7f || buf[0] == 8){\
						if(counter > 0){\
							counter--;\
							buffer[counter] = 0;\
							const_write(client, "\x08 \x08");\
						}\
					}else{\
						write(client, buf, 1);\
						buffer = realloc(buffer, counter + 2);\
						buffer[counter] = buf[0];\
						buffer[counter + 1] = 0;\
						counter++;\
					}\
				}\
				const_write(client, "\r\n");\
				sendto = buffer;\
			}
#define EDIT		{\
				char* path = sstrcat3(lbbs_info.password_database_path, "/", sendto);\
				char exists = lbbs_exists(path);\
				free(path);\
				if(!exists){\
					free(sendto);\
					const_write(client, "User doesn't exist.\r\n");\
					WAIT_UNTIL_KEY;\
					REFRESH;\
					continue;\
				}\
			}\
			editor(client, username);\
			char* _tmp = sstrcat(lbbs_info.workplace_path, "/");\
			char* from = sstrcat(_tmp, username);\
			free(_tmp);\
			_tmp = sstrcat(edit_path, "/");\
			char* _tmp2 = sstrcat(_tmp, sendto);\
			free(_tmp);\
			char* date = malloc(128);\
			time_t t = time(NULL);\
			struct tm *time_st = localtime(&t);\
			strftime(date, 128, "%Y%m%d-%H%M%S", time_st);\
			char* filename = sstrcat(date, username);\
			char* _tmp3 = sstrcat("/", filename);\
			char* to = sstrcat(_tmp2, _tmp3);\
			free(_tmp2);\
			free(_tmp3);\
			free(filename);\
			char exists = lbbs_exists(from);\
			if(exists){\
				rename(from, to);\
			}\
			free(date);\
			free(from);\
			free(to);\
			free(sendto);

#define EDIT_END	WAIT_UNTIL_KEY;\
			REFRESH;

void menu(int client, char* username){
	int outfd = dup(1);
refresh:
	dup2(client, 1);
	/*
	printf(" #     #    ##    #####  ##   #\r\n");
	printf(" ##   ##   #  #     #    # #  #\r\n");
	printf(" # # # #  ######    #    #  # #\r\n");
	printf(" #  #  #  #    #  #####  #   ##\x1b[0m (LineBBS %.2f)\r\n", LBBS_VERSION);
	*/
	printf(" \x1b[44m\x1b[37m\x1b[1m \x1b[0m     \x1b[44m\x1b[37m\x1b[1m \x1b[0m    \x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m    \x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m  \x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m   \x1b[44m\x1b[37m\x1b[1m \x1b[0m\r\n");
	printf(" \x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m   \x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m   \x1b[44m\x1b[37m\x1b[1m \x1b[0m  \x1b[44m\x1b[37m\x1b[1m \x1b[0m     \x1b[44m\x1b[37m\x1b[1m \x1b[0m    \x1b[44m\x1b[37m\x1b[1m \x1b[0m \x1b[44m\x1b[37m\x1b[1m \x1b[0m  \x1b[44m\x1b[37m\x1b[1m \x1b[0m\r\n");
	printf(" \x1b[44m\x1b[37m\x1b[1m \x1b[0m \x1b[44m\x1b[37m\x1b[1m \x1b[0m \x1b[44m\x1b[37m\x1b[1m \x1b[0m \x1b[44m\x1b[37m\x1b[1m \x1b[0m  \x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m    \x1b[44m\x1b[37m\x1b[1m \x1b[0m    \x1b[44m\x1b[37m\x1b[1m \x1b[0m  \x1b[44m\x1b[37m\x1b[1m \x1b[0m \x1b[44m\x1b[37m\x1b[1m \x1b[0m\r\n");
	printf(" \x1b[44m\x1b[37m\x1b[1m \x1b[0m  \x1b[44m\x1b[37m\x1b[1m \x1b[0m  \x1b[44m\x1b[37m\x1b[1m \x1b[0m  \x1b[44m\x1b[37m\x1b[1m \x1b[0m    \x1b[44m\x1b[37m\x1b[1m \x1b[0m  \x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m  \x1b[44m\x1b[37m\x1b[1m \x1b[0m   \x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[44m\x1b[37m\x1b[1m \x1b[0m\x1b[1m (LineBBS %.3f)\x1b[0m\r\n", LBBS_VERSION);
	dup2(outfd, 1);
	{
		int i;
		for(i = 0; i < 79 - (lbbs_info.top_text == NULL ? 0 : (1 + 1 + 3 + strlen(lbbs_info.top_text))); i++) write(client, "\x1b[1m=\x1b[0m", 9);
	}
	if(lbbs_info.top_text != NULL){
		const_write(client, " ");
		const_write(client, lbbs_info.top_text);
		const_write(client, " \x1b[1m===\x1b[0m");
	}
	write(client, "\r\n", 2);
	const_write(client, "\x1b[34m[\x1b[1m\x1b[33ml\x1b[m\x1b[34m]\x1b[m Logout                \x1b[34m[\x1b[1m\x1b[33mv\x1b[m\x1b[34m]\x1b[m Version          \x1b[34m[\x1b[1m\x1b[33mc\x1b[m\x1b[34m]\x1b[m Calender\r\n");
	const_write(client, "\x1b[34m[\x1b[1m\x1b[33mr\x1b[m\x1b[34m]\x1b[m Refresh Menu          \x1b[34m[\x1b[1m\x1b[33mm\x1b[m\x1b[34m]\x1b[m Mail             \x1b[34m[\x1b[1m\x1b[33mt\x1b[m\x1b[34m]\x1b[m Server time\r\n");
	const_write(client, "\x1b[34m[\x1b[1m\x1b[33mR\x1b[m\x1b[34m]\x1b[m Read Mail             \x1b[34m[\x1b[1m\x1b[33mL\x1b[m\x1b[34m]\x1b[m List Users       \x1b[34m[\x1b[1m\x1b[33md\x1b[m\x1b[34m]\x1b[m Delete your account\r\n");
	if(strcmp(username, "sysop") == 0){
		char* text = "=== SysOp Menu ";
		const_write(client, text);
		int i;
		for(i = 0; i < 79 - strlen(text); i++) const_write(client, "=");
		const_write(client, "\r\n");
		const_write(client, "\x1b[33m\x1b[1mYOU ARE USING SYSOP RIGHT NOW. BE CAREFUL.\x1b[m\r\n");
		const_write(client, "\x1b[34m[\x1b[1m\x1b[33mb\x1b[m\x1b[34m]\x1b[m Ban User              \x1b[34m[\x1b[1m\x1b[33mu\x1b[m\x1b[34m]\x1b[m Unban User       \x1b[34m[\x1b[1m\x1b[33ms\x1b[m\x1b[34m]\x1b[m Shutdown the server\r\n");
	}
	char* key = malloc(1);
	while(0 < read(client, key, 1)){
		if(key[0] == 'l'){
			const_write(client, "Logout - Goodbye!\r\n");
			shutdown(client, SHUT_RDWR);
			close(client);
			return;
		}else if(key[0] == 'r'){
			REFRESH;
		}else if(key[0] == 'v'){
			dup2(client, 1);
			printf("LineBBS v%.3f\r\n", LBBS_VERSION);
			dup2(outfd, 1);
			WAIT_UNTIL_KEY;
			REFRESH;
		}else if(key[0] == 'm'){
			const_write(client, "Username? ");
			char* sendto;
			EDIT_ASK;
			{
				char* path = sstrcat3(lbbs_info.password_database_path, "/", sendto);
				char exists = lbbs_exists(path);
				free(path);
				if(!exists){
					free(sendto);
					const_write(client, "User doesn't exist.\r\n");
					WAIT_UNTIL_KEY;
					REFRESH;
					continue;
				}
			}
			char* _tmp = sstrcat(lbbs_info.mail_path, "/");
			edit_path = sstrcat(_tmp, sendto);
			free(_tmp);
			char* date = malloc(128);\
			time_t t = time(NULL);\
			struct tm *time_st = localtime(&t);\
			strftime(date, 128, "%Y%m%d-%H%M%S", time_st);\
			char* data = sstrcat(date, username);
			free(date);
			editor(client, data);
			free(data);
			free(edit_path);
			EDIT_END;
		}else if(key[0] == 'c'){
			int fd[2];
			pipe(fd);
			int pid = fork();
			if(pid == 0){
				close(fd[0]);
				dup2(fd[1], 1);
				dup2(fd[1], 2);
				execlp("cal", "cal", NULL);
				_exit(0);
			}else{
				close(fd[1]);
				char* chr = malloc(1);
				waitpid(fd[0], 0, 0);
				while(read(fd[0], chr, 1) > 0){
					if(chr[0] == '\n'){
						write(client, "\r\n", 2);
					}else{
						write(client, chr, 1);
					}
				}
				free(chr);
				const_write(client, "\x1b[1A");
				WAIT_UNTIL_KEY;
				REFRESH;
			}
		}else if(key[0] == 't'){
			char* _time = lbbs_date();
			const_write(client, _time);
			write(client, "\r\n", 2);
			free(_time);
			WAIT_UNTIL_KEY;
			REFRESH;
		}else if(key[0] == 'R'){
mail_read:
			clear(client);
			char* _tmp = sstrcat(lbbs_info.mail_path, "/");
			char* path = sstrcat(_tmp, username);
			free(_tmp);
			char** files = malloc(sizeof(*files));
			files[0] = NULL;
			DIR* d;
			struct dirent* dir;
			d = opendir(path);
			int count = 0;
			dup2(client, 1);
			if(d){
				while((dir = readdir(d)) != NULL){
					if(strcmp(dir->d_name, "..") == 0 || strcmp(dir->d_name, ".") == 0) continue;
					files = realloc(files, sizeof(*files) * (count + 2));
					files[count] = strdup(dir->d_name);
					files[count + 1] = NULL;
					char* year = malloc(5);
					char* month = malloc(3);
					char* day = malloc(3);
					char* hour = malloc(3);
					char* minute = malloc(3);
					char* second = malloc(3);
					char* person = malloc(strlen(dir->d_name) - 4 - 2 - 2 - 2 - 2 - 2 - 1 + 1);
					year[4] = 0;
					month[2] = 0;
					day[2] = 0;
					hour[2] = 0;
					minute[2] = 0;
					second[2] = 0;
					person[strlen(dir->d_name) - 4 - 2 - 2 - 2 - 2 - 2 - 1] = 0;
					memcpy(year, dir->d_name, 4);
					memcpy(month, dir->d_name + 4, 2);
					memcpy(day, dir->d_name + 4 + 2, 2);
					memcpy(hour, 1 + dir->d_name + 4 + 2 + 2, 2);
					memcpy(minute, 1 + dir->d_name + 4 + 2 + 2 + 2, 2);
					memcpy(second, 1 + dir->d_name + 4 + 2 + 2 + 2 + 2, 2);
					memcpy(person, 1 + dir->d_name + 4 + 2 + 2 + 2 + 2 + 2, strlen(dir->d_name) - 4 - 2 - 2 - 2 - 2 - 2);
					printf("%d: %s/%s/%s %s:%s:%s - %s\r\n", count + 1, year, month, day, hour, minute, second, person);
					free(year);
					free(month);
					free(day);
					free(hour);
					free(minute);
					free(second);
					free(person);
					count++;
				}
				closedir(d);
			}
			int dont_goto = 0;
			printf("Total: %d mail(s)\r\n", count);
			const_write(client, "\x1b[34m[\x1b[1m\x1b[33mr\x1b[m\x1b[34m]\x1b[m Read                  \x1b[34m[\x1b[1m\x1b[33md\x1b[m\x1b[34m]\x1b[m Delete                \x1b[34m[\x1b[1m\x1b[33mq\x1b[m\x1b[34m]\x1b[m Quit                  \r\n");
			const_write(client, "\x1b[34m[\x1b[1m\x1b[33mR\x1b[m\x1b[34m]\x1b[m Refresh               \r\n"); 
tryagain:
			read(client, key, 1);
			if(key[0] == '\r'){
				read(client, key, 1);
				goto tryagain;
			}else if(key[0] == 'r'){
				_tmp = sstrcat(path, "/");
				int counter = 0;
				int number = 0;
				char* buffer = malloc(0);
tryagain_read:
				const_write(client, "Select a mail with the number: ");
				while(true){
					read(client, key, 1);
					if(key[0] == '\r'){
						write(client, "\r\n", 2);
						read(client, key, 1);
						break;
					}else if(key[0] == 0x7f || key[0] == 8){
						if(counter > 0){
							buffer[counter - 1] = 0;
							write(client, "\x08 \x08", 3);
							counter--;
						}
					}else if(key[0] >= '0' && key[0] <= '9'){
						buffer = realloc(buffer, counter + 2);
						buffer[counter] = key[0];
						buffer[counter + 1] = 0;
						write(client, key, 1);
						counter++;
					}else if(key[0] == 'q'){
						goto skiptype;
					}
				}
				if(atoi(buffer) == 0 || atoi(buffer) > count){
					const_write(client, "Invalid number ; try again\r\n");
					counter = 0;
					buffer = realloc(buffer, 0);
					goto tryagain_read;
				}
				clear(client);
				number = atoi(buffer) - 1;
				char* fullpath = sstrcat(_tmp, files[number]);
				lbbs_more(client, fullpath);
				free(_tmp);
				free(buffer);
				free(fullpath);
				int i;
				for(i = 0; i < 79; i++) write(client, "-", 1);
				const_write(client, "\r\n");
				const_write(client, "Reply? ");
ask_yorn:
				read(client, key, 1);
				if(key[0] == 'y' || key[0] == 'Y'){
					const_write(client, "Y\r\n");
					char* person = malloc(strlen(files[number]) - 4 - 2 - 2 - 2 - 2 - 2 - 1 + 1);
					person[strlen(files[number]) - 4 - 2 - 2 - 2 - 2 - 2 - 1] = 0;
					memcpy(person, 1 + files[number]	+ 4 + 2 + 2 + 2 + 2 + 2, strlen(files[number]) - 4 - 2 - 2 - 2 - 2 - 2);
					char* sendto = person;
					_tmp = sstrcat(lbbs_info.mail_path, "/");
					edit_path = sstrcat(_tmp, sendto);
					free(_tmp);
					char* date = malloc(128);
					time_t t = time(NULL);
					struct tm *time_st = localtime(&t);
					strftime(date, 128, "%Y%m%d-%H%M%S", time_st);
					char* data = sstrcat(date, username);
					free(date);
					editor(client, data);
					free(data);
					free(edit_path);
					goto skiptype;
				}else if(key[0] == 'n' || key[0] == 'N'){
					const_write(client, "N\r\n");
				}else if(key[0] == '\r'){
					read(client, key, 1);
					goto ask_yorn;
				}else{
					goto ask_yorn;
				}

			}else if(key[0] == 'd'){
				_tmp = sstrcat(path, "/");
				int counter = 0;
				int number = 0;
				char* buffer = malloc(0);
tryagain_delete:
				const_write(client, "Select a mail with the number: ");
				while(true){
					read(client, key, 1);
					if(key[0] == '\r'){
						write(client, "\r\n", 2);
						read(client, key, 1);
						break;
					}else if(key[0] == 8 || key[0] == 0x7f){
						if(counter > 0){
							buffer[counter - 1] = 0;
							write(client, "\x08 \x08", 3);
							counter--;
						}
					}else if(key[0] >= '0' && key[0] <= '9'){
						buffer = realloc(buffer, counter + 2);
						buffer[counter] = key[0];
						buffer[counter + 1] = 0;
						write(client, key, 1);
						counter++;
					}else if(key[0] == 'q'){
						goto skiptype;
					}
				}
				if(atoi(buffer) == 0 || atoi(buffer) > count){
					const_write(client, "Invalid number ; try again\r\n");
					counter = 0;
					buffer = realloc(buffer, 0);
					goto tryagain_delete;
				}
				number = atoi(buffer) - 1;
				char* fullpath = sstrcat(_tmp, files[number]);
				remove(fullpath);
				free(_tmp);
				free(fullpath);
				printf("Deleted successfully\r\n");
			}else if(key[0] == 'q'){
				int i;
				for(i = 0; files[i] != NULL; i++){
					free(files[i]);
				}
				free(files);
				dup2(outfd, 1);
				free(path);
				REFRESH;
				return;
			}else if(key[0] == 'R'){
				goto skiptype;
			}else{
				goto tryagain;
			}
skiptype:;
			int i;
			for(i = 0; files[i] != NULL; i++){
				free(files[i]);
			}
			free(files);
			dup2(outfd, 1);
			free(path);
			goto mail_read;
			return;
		}else if(key[0] == 'L'){
			DIR* d;
			struct dirent* dir;
			d = opendir(lbbs_info.password_database_path);
			int count = 0;
			int count2 = 0;
			if(d){
				while((dir = readdir(d)) != NULL){
					if(strcmp(dir->d_name, "..") == 0 || strcmp(dir->d_name, ".") == 0) continue;
					const_write(client, dir->d_name);
					if(strcmp(dir->d_name, username) == 0){
						const_write(client, " (You)");
					}
					const_write(client, "\r\n");
					count2++;
					if(count2 == 23){
						count2 = 0;
						const_write(client, "--MORE--");
						WAIT_UNTIL_KEY;
						clear(client);
					}
					count++;
				}
			}
			dup2(client, 1);
			printf("Total: %d user(s)\r\n", count);
			dup2(outfd, 1);
			WAIT_UNTIL_KEY;
			REFRESH;
		}else if(key[0] == 'd'){
			const_write(client, "Are you sure you want to delete your account? ");
delete_yorn:
			read(client, key, 1);
			if(key[0] == '\r'){
				read(client, key, 1);
				goto delete_yorn;
			}else if(key[0] == 'Y' || key[0] == 'y'){
				const_write(client, "\r\n");
				const_write(client, "Deleting your account ... ");
				DIR* d;
				struct dirent* dir;
				char* _tmp = sstrcat(lbbs_info.mail_path, "/");
				char* buffer = sstrcat(_tmp, username);
				free(_tmp);
				d = opendir(buffer);
				if(d){
					while((dir = readdir(d)) != NULL){
						if(strcmp(dir->d_name, "..") == 0 || strcmp(dir->d_name, ".") == 0) continue;
						_tmp = sstrcat(buffer, "/");
						char* path = sstrcat(_tmp, dir->d_name);
						free(_tmp);
						remove(path);
					}
				}
				rmdir(buffer);
				free(buffer);
				_tmp = sstrcat(lbbs_info.password_database_path, "/");
				buffer = sstrcat(_tmp, username);
				remove(buffer);
				free(_tmp);
				free(buffer);
				const_write(client, "Done.\r\n");
				const_write(client, "Goodbye... :(\r\n");
				shutdown(client, SHUT_RDWR);
				_exit(0);
			}else if(key[0] == 'N' || key[0] == 'n'){
				const_write(client, "\r\n");
			}else{
				goto delete_yorn;
			}
		}else if(strcmp(username, "sysop") == 0){
			if(key[0] == 'b'){
				const_write(client, "Username? ");
				char* sendto;
				edit_path = lbbs_info.banned_database_path;
				EDIT_ASK;
				{
					char* path = sstrcat3(lbbs_info.password_database_path, "/", sendto);
					char exists = lbbs_exists(path);
					free(path);
					if(!exists){
						free(sendto);
						const_write(client, "User doesn't exist.\r\n");
						WAIT_UNTIL_KEY;
						REFRESH;
						continue;
					}
				}
				editor(client, sendto);
				WAIT_UNTIL_KEY;
				REFRESH;
			}else if(key[0] == 'u'){
				const_write(client, "Username? ");
				char* sendto;
				edit_path = lbbs_info.banned_database_path;
				EDIT_ASK;
				{
					char* path = sstrcat3(lbbs_info.password_database_path, "/", sendto);
					char exists = lbbs_exists(path);
					free(path);
					if(!exists){
						free(sendto);
						const_write(client, "User doesn't exist.\r\n");
						WAIT_UNTIL_KEY;
						REFRESH;
						continue;
					}
				}
				const_write(client, "Are you sure? ");
unban_again:
				read(client, key, 1);
				if(key[0] == 'y' || key[0] == 'Y'){
					const_write(client, "Y\r\n");
					char* _tmp = sstrcat(lbbs_info.banned_database_path, "/");
					char* path = sstrcat(_tmp, sendto);
					if(!remove(path)) const_write(client, "Unbanned.\r\n");
				}else if(key[0] == 'n' || key[0] == 'N'){
					const_write(client, "N\r\n");
				}else if(key[0] == '\r'){
					read(client, key, 1);
					goto unban_again;
				}else{
					goto unban_again;
				}
				free(sendto);
				WAIT_UNTIL_KEY;
				REFRESH;
				continue;
			}else if(key[0] == 's'){
				const_write(client, "Are you sure? ");
shutdown_again:
				read(client, key, 1);
				if(key[0] == 'y' || key[0] == 'Y'){
					const_write(client, "Y\r\n");
					kill(getppid(), SIGUSR1);
				}else if(key[0] == 'n' || key[0] == 'N'){
					const_write(client, "N\r\n");
					WAIT_UNTIL_KEY;
					REFRESH;
				}else if(key[0] == '\r'){
					read(client, key, 1);
					goto shutdown_again;
				}else{
					goto shutdown_again;
				}
			}
		}else if(key[0] == '\r'){
			read(client, key, 1);
		}
	}
}

void sigusr1(int sig){
	kill(getppid(), SIGUSR1);
}

int client_sock;
void sigpipe(int sig){
	shutdown(client_sock, SHUT_RDWR);
	_exit(0);
}

void client_handler(struct sockaddr_in address, int client, int pipefd){
	char* chars = malloc(3);
	write(client, "\xFF\xFD\x01", 3);
	read(client, chars, 3);
	write(client, "\xFF\xFB\x01", 3);
	read(client, chars, 3);
	write(client, "\xFF\xFD\x22", 3);
	read(client, chars, 3);
	clear(client);
	signal(SIGPIPE, sigpipe);
	signal(SIGUSR1, sigusr1);
	client_sock = client;
	if(lbbs_info.welcome != NULL){
		FILE* welcome_file = fopen(lbbs_info.welcome, "r");
		if(welcome_file != NULL){
			const_write(client, "\x1b[1m\x1b[44m");
			write(client, "================================ WELCOME MESSAGE ==============================\r\n", 81);
			while(!feof(welcome_file)){
				char* line = NULL;
				size_t linesize = 0;
				ssize_t linelen = getline(&line, &linesize, welcome_file);
				if(linelen == -1) break;
				line[linelen - 1] = '\r';
				write(client, line, linelen - 1);
				int i;
				for(i = 0; i < 79 - linelen + 1; i++) write(client, " ", 1);
				write(client, "\r\n", 2);
				free(line);
			}
			fclose(welcome_file);
			int i;
			for(i = 0; i < 79; i++) write(client, "=", 1);
			write(client, "\r\n", 2);
			const_write(client, "\x1b[0m");
		}
	}
	if(lbbs_info.warn != NULL){
		int i;
		for(i = 0; i < (80 - strlen(lbbs_info.warn)) / 2; i++) const_write(client, " ");
		const_write(client, "\x1b[5m\x1b[6m\x1b[1m\x1b[33m\x1b[41m ");
		const_write(client, lbbs_info.warn);
		const_write(client, " \x1b[m\r\n");
	}
	const_write(client, "Login to your account with typing your username and password.\r\n");
	if(lbbs_info.create_account) const_write(client, "Type `NEW` on Username to create a new account.\r\n");
	int outfd = dup(1);
	dup2(client, 1);
	char* hostname = malloc(128);
	gethostname(hostname, 128);
	printf("\r\nLineBBS/%.3f (%s) login\r\nANSI Terminal + 80x[any height] is required.\r\n", LBBS_VERSION, hostname);
	free(hostname);
	dup2(outfd, 1);
	const_write(client, "login: ");
	unsigned char* buf = malloc(1);
	char* username = malloc(2);
	memset(username, 0, 2);
	int counter = 0;
	int skipcount = 0;
	char sb = 0;
	while(true){
		if(1 > read(client, buf, 1)) _exit(0);
		if(sb){
			if(buf[0] == 240){
				skipcount = 0;
				sb = 0;
				const_write(client, "\x08 \x08");
			}
			continue;
		}
		if(skipcount != 0){
			if(skipcount == 2 && buf[0] == 250){
				sb = 1;
			}
			skipcount--;
			continue;
		}
		username = realloc(username, counter + 2);
		if(buf[0] == 0x7f || buf[0] == 0x08){
			if(counter > 0){
				counter--;
				username[counter] = 0;
				const_write(client, "\x08 \x08");
			}
		}else if(buf[0] == '\r'){
			read(client, chars, 1);
			if(counter == 0){
				continue;
			}
			break;
		}else if(buf[0] == 0xff){
			skipcount = 2;
		}else if(buf[0] > 0 && buf[0] < 0xff){
			write(client, buf, 1);
			username[counter] = buf[0];
			username[counter + 1] = 0;
			counter++;
		}
	}
	write(client, "\r\n", 2);
	if(strcmp(username, "NEW") == 0){
		if(!lbbs_info.create_account){
			const_write(client, "Creating an account is not allowed in this server.\r\n");
			if(lbbs_info.contact != NULL){
				const_write(client, "Contact ");
				const_write(client, lbbs_info.contact);
				const_write(client, " to get an account.\r\n");
			}
			shutdown(client, SHUT_RDWR);
			_exit(0);
		}
		clear(client);
		const_write(client, "Creating a new account.\r\n");
try_username:
		free(username);
		username = malloc(2);
		memset(username, 0, 2);
		counter = 0;
		sb = 0;
		skipcount = 0;
		const_write(client, "Username: ");
		while(true){
			if(1 > read(client, buf, 1)) _exit(0);
			if(sb){
				if(buf[0] == 240){
					skipcount = 0;
					sb = 0;
				}
				continue;
			}
			if(skipcount != 0){
				if(skipcount == 2 && buf[0] == 250){
					sb = 1;
				}
				skipcount--;
				continue;
			}
			username = realloc(username, counter + 2);
			if(buf[0] == 0x7f || buf[0] == 0x08){
				if(counter > 0){
					counter--;
					username[counter] = 0;
					const_write(client, "\x08 \x08");
				}
			}else if(buf[0] == 0xff){
				skipcount = 2;
			}else if(buf[0] == 13){
				break;
			}else if(buf[0] == 10){
				break;
			}else if(buf[0] > 0 && buf[0] < 0xff){
				write(client, buf, 1);
				username[counter] = buf[0];
				username[counter + 1] = 0;
				counter++;
			}
		}
		{
			char* path = sstrcat3(lbbs_info.password_database_path, "/", username);
			char exists = lbbs_exists(path);
			free(path);
			if(exists){
				const_write(client, "\r\nUser already exists. try again.\r\n");
				goto try_username;
			}
		}
		read(client, chars, 1);
		const_write(client, "\r\n");
		const_write(client, "Password: ");
		char* password = malloc(2);
		password[0] = 0;
		counter = 0;
		sb = 0;
		skipcount = 0;
		while(true){
			if(1 > read(client, buf, 1)) _exit(0);
			if(sb){
				if(buf[0] == 240){
					skipcount = 0;
					sb = 0;
				}
				continue;
			}
			if(skipcount != 0){
				if(skipcount == 2 && buf[0] == 250){
					sb = 1;
				}
				skipcount--;
				continue;
			}
			password = realloc(password, counter + 2);
			if(buf[0] == 0x08 || buf[0] == 0x7f){
				if(counter > 0){
					counter--;
					password[counter] = 0;
					const_write(client, "\x08 \x08");
				}
			}else if(buf[0] == 13){
				break;
			}else if(buf[0] == 10){
				break;
			}else if(buf[0] == 0xff){
				skipcount = 2;
			}else if(buf[0] > 0 && buf[0] < 0xff){
				write(client, "*", 1);
				password[counter] = buf[0];
				password[counter + 1] = 0;
				counter++;
			}
		}
		read(client, chars, 1);
		const_write(client, "\r\nAccount has been made. Reconnect and login.\r\n");
		char* path = sstrcat3(lbbs_info.password_database_path, "/", username);
		FILE* db_file = fopen(path, "w");
		fwrite(password, 1, strlen(password), db_file);
		fclose(db_file);
		free(path);

		path = sstrcat3(lbbs_info.mail_path, "/", username);
		mkdir(path, 0770);
		free(path);
		shutdown(client, SHUT_RDWR);
		close(client);
		return;
	}else{
		{
			char* path = sstrcat3(lbbs_info.banned_database_path, "/", username);
			char exists = lbbs_exists(path);
			if(exists){
				struct stat s;
				stat(path, &s);
				FILE* f = fopen(path, "r");
				const_write(client, "You are banned from the server.\r\n");
				const_write(client, "==================================== REASON ===================================\r\n");
				char* reason = malloc(1);
				reason[s.st_size] = 0;
				int x = 0;
				int y = 0;
				off_t i;
				for(i = 0; i < s.st_size; i++){
					fread(reason, 1, 1, f);
					if(reason[0] == '\n'){
						write(client, "\r\n", 2);
						y++;
						x = 0;
					}else{
						write(client, reason, 1);
						x++;
						if(x == 79){
							x = 0;
							y++;
						}
					}
					if(y == 23){
						const_write(client, "--MORE--");
						char* key = malloc(1);
						WAIT_UNTIL_KEY;
						free(key);
						clear(client);
						x = 0;
						y = 0;
					}
				}
				fclose(f);
				free(reason);
				const_write(client, "\r\n\r\n");
				if(lbbs_info.contact != NULL){
					const_write(client, "Try contacting ");
					const_write(client, lbbs_info.contact);
					const_write(client, ". Maybe you can get unbanned.\n\r\n");
				}
				shutdown(client, SHUT_RDWR);
				free(path);
				_exit(0);
			}
			free(path);
		}
		int failure = 0;
try_passwd:
		const_write(client, "Password: ");
		char* password = malloc(2);
		counter = 0;
		while(true){
			if(1 > read(client, buf, 1)) _exit(0);
			if(sb){
				if(buf[0] == 240){
					skipcount = 0;
					sb = 0;
				}
				continue;
			}
			if(skipcount != 0){
				if(skipcount == 2 && buf[0] == 250){
					sb = 1;
				}
				skipcount--;
				continue;
			}
			password = realloc(password, counter + 2);
			if(buf[0] == 0x7f || buf[0] == 0x08){
				if(counter > 0){
					counter--;
					password[counter] = 0;
					const_write(client, "\x08 \x08");
				}
			}else if(buf[0] == '\r'){
				read(client, chars, 1);
				break;
			}else if(buf[0] == 0xff){
				skipcount = 2;
			}else if(buf[0] > 0 && buf[0] < 0xff){
				write(client, "*", 1);
				password[counter] = buf[0];
				password[counter + 1] = 0;
				counter++;
			}
		}
		write(client, "\r\n", 2);
		write(client, "\x1b[0m", 4);
		FILE* f;
		struct stat s;
		{
			char* path = sstrcat3(lbbs_info.password_database_path, "/", username);
			char exists = lbbs_exists(path);
			stat(path, &s);
			f = fopen(path, "r");
			free(path);
			if(!exists){
				const_write(client, "User doesn't exist. Reconnect.\r\n");
				shutdown(client, SHUT_RDWR);
				return;
			}
		}
		char* passwd_correct = malloc(s.st_size + 1);
		passwd_correct[s.st_size] = 0;
		fread(passwd_correct, 1, s.st_size, f);
		if(strcmp(password, passwd_correct) == 0){
			free(passwd_correct);
			fclose(f);
			clear(client);
			const_write(client, "\x1b[1mWelcome back to the server, ");
			const_write(client, username);
			const_write(client, "!\x1b[0m\r\n");
			int i;
			for(i = 0; i < 79; i++) write(client, "\x1b[1m=\x1b[0m", 9);
			write(client, "\r\n", 2);
			menu(client, username);
		}else{
			free(passwd_correct);
			fclose(f);
			failure++;
			const_write(client, "Wrong password.\r\n");
			if(failure == 3){
				const_write(client, "3 failures. Reconnect.\r\n");
				shutdown(client, SHUT_RDWR);
				_exit(0);
			}else{
				const_write(client, "Try again.\r\n");
			}
			free(password);
			goto try_passwd;
		}
	}
}

int* sockets;
int socket_count = 0;

void kill_sockets(int sig){
	int i;
	for(i = 0; i < socket_count; i++){
		shutdown(sockets[i], SHUT_RDWR);
		printf("%d ", sockets[i]);
	}
	printf("\n");
	exit(0);
}

int lbbs_success_exit = 0;

void lbbs_launch(uint16_t port, char* config){
	sockets = malloc(sizeof(*sockets));
	lbbs_info.welcome = NULL;
	lbbs_info.password_database_path = NULL;
	lbbs_info.banned_database_path = NULL;
	lbbs_info.workplace_path = NULL;
	lbbs_info.mail_path = NULL;
	lbbs_info.warn = NULL;
	lbbs_info.contact = NULL;
	lbbs_info.top_text = NULL;
	lbbs_info.create_account = 1;
	lbbs_info.le_path = strdup("le");
	char* config_path = strdup(config);
	free(config);
	printf("Config Path = %s\n", config_path);
	printf("Checking if it exists ... ");
	if(lbbs_exists(config_path)){
		printf("found\n");
	}else{
		printf("not found\n");
		fprintf(stderr, "Config file not found. Exiting.\n");
		return;
	}
	printf("Checking if it is a file ... ");
	if(S_ISREG(lbbs_mode(config_path))){
		printf("yes\n");
	}else{
		printf("no\n");
		fprintf(stderr, "Config file must be a file. Exiting.\n");
		return;
	}
	FILE* f = fopen(config_path, "r");
	enum {
		LBBS_STATE_NONE = 0,
		LBBS_STATE_SERVER,
		LBBS_STATE_DATABASE
	};
	char state = LBBS_STATE_NONE;
	char* cwd = NULL;
	cwd = getcwd(cwd, 1);
	while(!feof(f)){
		char* line = NULL;
		size_t linesize = 0;
		ssize_t linelen = getline(&line, &linesize, f);
		if(linelen == -1) break;
		line[linelen - 1] = 0;
		if(strlen(line) > 0 && line[0] == '#'){
		}else if(strcmp(line, "[Server]") == 0){
			state = LBBS_STATE_SERVER;
		}else if(strcmp(line, "[Database]") == 0){
			state = LBBS_STATE_DATABASE;
		}else{
			int i;
			for(i = 0; i < linelen - 1; i++){
				if(line[i] == '='){
					line[i] = 0;
					break;
				}
			}
			char* key = line;
			char* value = key + strlen(key) + 1;
			if(strcmp(key, "ChangeDirectory") == 0){
				char* dir;
				if(value[0] == '+'){
					dir = sstrcat3(cwd, "/", value + 1);
				}else{
					dir = strdup(value);
				}
				if(!lbbs_exists(dir)){
					fprintf(stderr, "ChangeDirectory: %s: Directory is nonexistent\n", dir);
					return;
				}
				chdir(dir);
				free(dir);
				free(cwd);
				cwd = NULL;
				cwd = getcwd(cwd, 1);
			}else if(state == LBBS_STATE_SERVER){
				if(strcmp(key, "Port") == 0){
					port = atoi(value);
				}else if(strcmp(key, "Welcome") == 0){
					if(lbbs_info.welcome != NULL) free(lbbs_info.welcome);
					if(value[0] == '+'){
						lbbs_info.welcome = sstrcat3(cwd, "/", value + 1);
					}else{
						lbbs_info.welcome = strdup(value);
					}
				}else if(strcmp(key, "WorkplacePath") == 0){
					if(lbbs_info.workplace_path != NULL) free(lbbs_info.workplace_path);
					if(value[0] == '+'){
						lbbs_info.workplace_path = sstrcat3(cwd, "/", value + 1);
					}else{
						lbbs_info.workplace_path = strdup(value);
					}
				}else if(strcmp(key, "AllowCreatingAccount") == 0){
					lbbs_info.create_account = strcmp(value, "yes") == 0;
				}else if(strcmp(key, "Contact") == 0){
					if(lbbs_info.contact != NULL) free(lbbs_info.contact);
					lbbs_info.contact = strdup(value);
				}else if(strcmp(key, "LePath") == 0){
					if(lbbs_info.le_path != NULL) free(lbbs_info.le_path);
					lbbs_info.le_path = strdup(value);
				}else if(strcmp(key, "Warning") == 0){
					if(lbbs_info.warn != NULL) free(lbbs_info.warn);
					lbbs_info.warn = strdup(value);
				}else if(strcmp(key, "TopText") == 0){
					if(lbbs_info.top_text != NULL) free(lbbs_info.top_text);
					lbbs_info.top_text = strdup(value);
				}
			}else if(state == LBBS_STATE_DATABASE){
				if(strcmp(key, "PasswordPath") == 0){
					if(lbbs_info.password_database_path != NULL) free(lbbs_info.password_database_path);
					if(value[0] == '+'){
						lbbs_info.password_database_path = sstrcat3(cwd, "/", value + 1);
					}else{
						lbbs_info.password_database_path = strdup(value);
					}
				}else if(strcmp(key, "BanPath") == 0){
					if(lbbs_info.banned_database_path != NULL) free(lbbs_info.banned_database_path);
					if(value[0] == '+'){
						lbbs_info.banned_database_path = sstrcat3(cwd, "/", value + 1);
					}else{
						lbbs_info.banned_database_path = strdup(value);
					}
				}else if(strcmp(key, "MailPath") == 0){
					if(lbbs_info.mail_path != NULL) free(lbbs_info.mail_path);
					if(value[0] == '+'){
						lbbs_info.mail_path = sstrcat3(cwd, "/", value + 1);
					}else{
						lbbs_info.mail_path = strdup(value);
					}
				}
			}
		}
		free(line);
	}
	free(cwd);
	printf("Checking if le exists ... ");
	int pid = fork();
	if(pid == 0){
		int nullfd = open("/dev/null", O_WRONLY);
		dup2(nullfd, 1);
		dup2(nullfd, 2);
		execlp(lbbs_info.le_path, lbbs_info.le_path, "-V", NULL);
		 close(nullfd);
		_exit(1);
	}else{
		int status;
		waitpid(pid, &status, 0);
		if(WEXITSTATUS(status) != 0){
			printf("not found\n");
			fprintf(stderr, "You need le\n");
			return;	
		}else{
			printf("found\n");
		}
	}
	fclose(f);
	if(port == 0){
		fprintf(stderr, "Port is not set\n");
		return;
	}
	if(lbbs_info.password_database_path == NULL){
		fprintf(stderr, "Password database path is not set\n");
		return;
	}
	if(!lbbs_exists(lbbs_info.password_database_path)){
		fprintf(stderr, "Password database doesn't exist\n");
		return;
	}else{
		if(!S_ISDIR(lbbs_mode(lbbs_info.password_database_path))){
			fprintf(stderr, "Password database needs to be a directory\n");
			return;
		}
	}
	if(lbbs_info.banned_database_path == NULL){
		fprintf(stderr, "Ban database path is not set\n");
		return;
	}
	if(!lbbs_exists(lbbs_info.banned_database_path)){
		fprintf(stderr, "Ban database doesn't exist\n");
		return;
	}else{
		if(!S_ISDIR(lbbs_mode(lbbs_info.banned_database_path))){
			fprintf(stderr, "Ban database needs to be a directory\n");
			return;
		}
	}
	if(lbbs_info.workplace_path == NULL){
		fprintf(stderr, "Workplace path is not set\n");
		return;
	}
	if(!lbbs_exists(lbbs_info.workplace_path)){
		fprintf(stderr, "Workplace doesn't exist\n");
		return;
	}else{
		if(!S_ISDIR(lbbs_mode(lbbs_info.workplace_path))){
			fprintf(stderr, "Workplace needs to be a directory\n");
			return;
		}
	}
	if(lbbs_info.mail_path == NULL){
		fprintf(stderr, "Mail path is not set\n");
		return;
	}
	if(!lbbs_exists(lbbs_info.mail_path)){
		fprintf(stderr, "Mail path doesn't exist\n");
		return;
	}else{
		if(!S_ISDIR(lbbs_mode(lbbs_info.mail_path))){
			fprintf(stderr, "Mail path needs to be a directory\n");
			return;
		}
	}
	if(lbbs_info.welcome != NULL){
		if(!lbbs_exists(lbbs_info.welcome)){
			fprintf(stderr, "Welcome file doesn't exist\n");
			return;
		}else{
			if(!S_ISREG(lbbs_mode(lbbs_info.welcome))){
					fprintf(stderr, "Mail path needs to be a regular file\n");
				return;
			}
		}
	}
	printf("Port = %d\n", port);
	printf("Creating the socket ... ");
	struct sockaddr_in addr;
	int server_socket = socket(AF_INET, SOCK_STREAM, 0);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr("0.0.0.0");
	if(server_socket < 0){
		printf("failed\n");
		return;
	}else{
		printf("success\n");
	}
	printf("Binding the socket ... ");
	int ret = bind(server_socket, (struct sockaddr*)&addr, sizeof(addr));
	if(ret < 0){
		printf("failed\n");
		return;
	}else{
		printf("success\n");
	}
	printf("Listening to the socket ... ");
	ret = listen(server_socket, 512);
	if(ret < 0){
		printf("failed\n");
		return;
	}else{
		printf("success\n");
	}
	printf("Server is ready. Started accepting\n");
	int daemonpid = fork();
	if(daemonpid == 0){
		FILE* pidfile = fopen("/var/run/linebbs.pid", "w");
		if(pidfile == NULL){
			fprintf(stderr, "%d: fopen() failed.\n", __LINE__);
			exit(-1);
		}
		fprintf(pidfile, "%d", getpid());
		fclose(pidfile);
		int logfd = open("/var/log/linebbs.log", O_RDWR | O_CREAT);
		if(logfd == -1){
			fprintf(stderr, "%d: open() failed.\n", __LINE__);
			exit(-1);
		}
		dup2(logfd, 1);
		/*
		 * dup2(logfd, 2);
		 */
	}else{
		printf("Parent process exiting.\n");
		printf("Daemon log = /var/log/linebbs.log\n");
		printf("Daemon PID = /var/run/linebbs.pid\n");
		lbbs_success_exit = 1;
		return;
	}
	signal(SIGCHLD, zombie_killer);
	signal(SIGPIPE, SIG_IGN);
	signal(SIGUSR1, kill_sockets);
	while(true){
		struct sockaddr_in client_address;
		socklen_t address_length = sizeof(client_address);
		int client_socket = accept(server_socket, (struct sockaddr*)&client_address, &address_length);
		sockets = realloc(sockets, socket_count + 1);
		sockets[socket_count] = client_socket;
		socket_count++;
		int pipefd[2];
		pipe(pipefd);
		if(fork() == 0){
			close(pipefd[1]);
			client_handler(client_address, client_socket, pipefd[0]);
			_exit(0);
		}else{
			close(pipefd[0]);
		}
	}
}
