#define _BSD_SOURCE

#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include <linebbs.h>

extern int lbbs_success_exit;

void usage(FILE* out, char** argv){
	fprintf(out, "%s [-hV] [-p port] [-c config]\n", argv[0]);
}

int main(int argc, char** argv){
	char flag;
	uint16_t port = 0;
	char* config = malloc(strlen("/etc/lbbs.conf") + 1);
	strcpy(config, "/etc/lbbs.conf");
	config[strlen("/etc/lbbs.conf")] = 0;
	while((flag = getopt(argc, argv, "hVp:c:")) != -1){
		if(flag == 'h'){
			usage(stdout, argv);
			return 0;
		}else if(flag == 'p'){
			port = atoi(optarg);
		}else if(flag == 'c'){
			free(config);
			config = strdup(optarg);
		}else if(flag == 'V'){
			printf("LineBBS - Line Editor BBS %.3f (compiled %s %s)\n", LBBS_VERSION, __DATE__, __TIME__);
			printf("Copyright (C) 2023 EEE++ tEam\n");
			printf("Copyright (C) 2023 re9177\n");
			return 0;
		}else{
			if(argv[optind] != NULL) break;
			if(strlen(argv[optind]) > 0 && argv[optind][0] != '-') break;
			usage(stderr, argv);
			return 1;
		}
	}
	printf("********** Starting up LineBBS %.3f **********\n", LBBS_VERSION);
	printf("Attempting to launch LineBBS\n");
	lbbs_launch(port, config);
	printf("***********************************************\n", LBBS_VERSION);
	return (lbbs_success_exit == 1 ? 0 : 1);
}
